import { Box, Card, Flex, Image } from '@chakra-ui/react';

const Fail = () => {
  return (
    <Box display="flex">
      <Image
        src={window.location.origin + '/Fail.jpg'}
        alt="Not Found"
        boxSize="100%"
        maxWeight="100vh"
        maxHeight="100vh"
      />
    </Box>
  );
};

export default Fail;
